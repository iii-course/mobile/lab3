package com.example.lab3_web;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ArrayList<String> urlsHistory = new ArrayList<String>();
    private int currentPageIndex = 0;
    Button searchButton, prevButton, nextButton;
    EditText urlInput;
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        searchButton = findViewById(R.id.searchButton);
        prevButton = findViewById(R.id.prevButton);
        nextButton = findViewById(R.id.nextButton);
        urlInput = findViewById(R.id.urlInput);
        webView = findViewById(R.id.webView);

        bindListenersToButtons();
        setupWebView();
    }

    private void bindListenersToButtons() {
        searchButton.setOnClickListener(this::onSearch);
        prevButton.setOnClickListener(this::onPrevPage);
        nextButton.setOnClickListener(this::onNextPage);
    }

    private void setupWebView() {
        webView.getSettings().setJavaScriptEnabled(true);
    }

    private void onNextPage(View view) {
        if (currentPageIndex < urlsHistory.size() - 1) {
            goToPage(currentPageIndex + 1);
        }
    }

    private void onPrevPage(View view) {
        if (currentPageIndex > 0) {
            goToPage(currentPageIndex - 1);
        }
    }

    private void onSearch(View view) {
        String url = getUrl();
        urlsHistory.add(url);
        int index = urlsHistory.size() - 1;
        goToPage(index);
    }

    private void goToPage(int index) {
        currentPageIndex = index;
        webView.loadUrl(urlsHistory.get(index));
    }

    private String getUrl() {
        return urlInput.getText().toString();
    }
}