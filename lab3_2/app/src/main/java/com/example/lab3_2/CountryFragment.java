package com.example.lab3_2;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class CountryFragment extends Fragment {
    TextView textView;
    ImageView imageView;

    public CountryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_country, container, false);
        textView = rootView.findViewById(R.id.textViewCountry);
        imageView = rootView.findViewById(R.id.imageCountry);
        return rootView;
    }

    public void setDescription(Country country) {
        switch (country) {
            case SWEDEN:
                textView.setText(R.string.Sweden_info);
                break;
            case DENMARK:
                textView.setText(R.string.Denmark_info);
                break;
            default:
                textView.setText(R.string.Norway_info);
        }
    }

    public void setImage(Country country) {
        switch (country) {
            case SWEDEN:
                imageView.setImageResource(R.drawable.flag_of_sweden);
                break;
            case DENMARK:
                imageView.setImageResource(R.drawable.flag_of_denmark);
                break;
            default:
                imageView.setImageResource(R.drawable.flag_of_norway);
        }
    }
}