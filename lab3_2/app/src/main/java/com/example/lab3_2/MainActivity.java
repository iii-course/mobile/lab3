package com.example.lab3_2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity
        implements OnSelectedCountyListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onCountrySelected(Country country) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        CountryFragment countryFragment = (CountryFragment) fragmentManager
                .findFragmentById(R.id.country_fragment);

        countryFragment.setDescription(country);
        countryFragment.setImage(country);
    }
}