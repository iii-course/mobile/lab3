package com.example.lab3_2;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class ButtonsFragment extends Fragment {
    Button buttonDenmark, buttonNorway, buttonSweden;

    public ButtonsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_buttons, container, false);
        buttonDenmark = rootView.findViewById(R.id.buttonDenmark);
        buttonNorway = rootView.findViewById(R.id.buttonNorway);
        buttonSweden = rootView.findViewById(R.id.buttonSweden);

        buttonDenmark.setOnClickListener(this::onClick);
        buttonNorway.setOnClickListener(this::onClick);
        buttonSweden.setOnClickListener(this::onClick);
        return rootView;
    }

    public void onClick(View view) {
        Country selectedCounty = mapIdToCountry(view.getId());
        OnSelectedCountyListener listener = (OnSelectedCountyListener) getActivity();
        listener.onCountrySelected(selectedCounty);
    }

    Country mapIdToCountry(int id) {
        Country country = Country.NORWAY;
        switch (id) {
            case R.id.buttonNorway:
                country = Country.NORWAY;
                break;
            case R.id.buttonSweden:
                country = Country.SWEDEN;
                break;
            case R.id.buttonDenmark:
                country = Country.DENMARK;
                break;
        }
        return country;
    }
}