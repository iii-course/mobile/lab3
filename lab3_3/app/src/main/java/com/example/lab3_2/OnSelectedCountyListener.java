package com.example.lab3_2;

public interface OnSelectedCountyListener {
    void onCountrySelected(Country country);
}
