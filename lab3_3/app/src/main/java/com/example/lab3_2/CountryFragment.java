package com.example.lab3_2;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class CountryFragment extends Fragment {
    ViewPager2 viewPager;
    FragmentPagerAdapter adapter;

    public CountryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_country, container, false);

        viewPager = (ViewPager2) rootView.findViewById(R.id.viewPager);
        adapter = new FragmentPagerAdapter(this);
        viewPager.setAdapter(adapter);

        return rootView;
    }

    private static class FragmentPagerAdapter extends FragmentStateAdapter {

        public FragmentPagerAdapter(@NonNull Fragment fragment) {
            super(fragment);
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            Fragment fragment;
            switch (position) {
                case 1:
                    fragment = new NorwayFragment();
                    break;
                case 2:
                    fragment = new SwedenFragment();
                    break;
                default:
                    fragment = new DenmarkFragment();
            }
            return fragment;
        }

        @Override
        public int getItemCount() {
            return 3;
        }
    }

    public void setCountry(Country country) {
        if (adapter != null) {
            int position = mapCountryToPosition(country);
            viewPager.setCurrentItem(position);
        }
    }

    private int mapCountryToPosition(Country country) {
        int position = 0;
        switch (country) {
            case NORWAY:
                position = 1;
                break;
            case SWEDEN:
                position = 2;
                break;
        }
        return position;
    }
}