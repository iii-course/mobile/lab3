package com.example.lab3_1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button buttonDenmark, buttonNorway, buttonSweden;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonDenmark = findViewById(R.id.buttonDenmark);
        buttonNorway = findViewById(R.id.buttonNorway);
        buttonSweden = findViewById(R.id.buttonSweden);

        buttonDenmark.setOnClickListener(this::onClickDenmark);
        buttonNorway.setOnClickListener(this::onClickNorway);
        buttonSweden.setOnClickListener(this::onClickSweden);
    }

    public void onClickDenmark(View v) {
        Intent intent = new Intent(this, DenmarkActivity.class);
        startActivity(intent);
    }

    public void onClickNorway(View v) {
        Intent intent = new Intent(this, NorwayActivity.class);
        startActivity(intent);
    }

    public void onClickSweden(View v) {
        Intent intent = new Intent(this, SwedenActivity.class);
        startActivity(intent);
    }
}